import React from 'react'
import Button from '@material-ui/core/Button'
import Modal from '@material-ui/core/Modal'
import CancelIcon from '@material-ui/icons/Cancel'
import '../App.css'

class MainComponent extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            mpdalOpen: false,
        }
        this.handleOpen = this.handleOpen.bind(this)
        this.handleClose = this.handleClose.bind(this)
    }

    handleOpen = () => {
        this.setState({modalOpen: true})
    }

    handleClose = () => {
        this.setState({modalOpen: false})
    }

    render() {
        return(
            <div align='center'>
                <Modal
                    open={this.state.modalOpen}
                    onClose={this.handleClose}  
                >
                    <div className='myModal'>
                        <p align='right'>
                            <Button onClick={this.handleClose}>
                                <CancelIcon />
                            </Button>
                        </p>
                        <h2 className="titleText">Войдите или зарегистрируйтесь</h2>
                        <div>
                            <Button className="btn" variant="outlined" >По телефону</Button>
                            <Button className="btn" variant="outlined">По e-mail</Button>
                        </div>
                        <p className="modalText">или</p>
                        <Button className="VKButton btn" variant="outlined">Войти через VK</Button>
                        <p className="privacyPolicy modalText">Регистрируясь в сервисе, вы принимаете условия и <br/>политику конфиденциальности</p>
                    </div>
                </Modal>
                <Button variant="contained" color="secondary" onClick={this.handleOpen}>
                    Я кнопка
                </Button>
            </div>
        )
    }
}

export default MainComponent