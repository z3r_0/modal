import React from 'react';
import ReactDOM from 'react-dom';
import MainComponent from './Components/Modal'
import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <MainComponent></MainComponent>
  </React.StrictMode>,
  document.getElementById('root')
);
